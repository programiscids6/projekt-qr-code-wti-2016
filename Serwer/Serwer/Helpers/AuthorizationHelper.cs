﻿using Serwer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Serwer.Helpers
{
    /// <summary>
    /// Klasa pomocnicza do celów autoryzacji użytkowników.
    /// </summary>
    public static class AuthorizationHelper
    {
        /// <summary>
        /// Pobieranie nazwy użytkownika z nagłówka HTTP
        /// </summary>
        /// <param name="header">Nagłówek HTTP</param>
        /// <returns>Nazwa użytkownika (string).</returns>
        public static string getUserName(HttpRequestHeaders header)
        {
            // Pobieranie danych autoryzacji z nagłówka HTTP
            AuthenticationHeaderValue authorization = header.Authorization;
            // Konwersja danych do postaci jawnej
            byte[] bytes = Convert.FromBase64String(authorization.Parameter);
            string plainCredentials = Encoding.ASCII.GetString(bytes);

            // Pobieranie nazwy użytkownika
            int colonIndex = plainCredentials.IndexOf(":");
            string userName = plainCredentials.Substring(0, colonIndex);
            return userName;
        }//getUserName()

        /// <summary>
        /// Pobieranie nazwy ID użytkownika z bazy na podstawie danych z nagłówka HTTP
        /// </summary>
        /// <param name="header">Nagłówek HTTP</param>
        /// <returns>Id użytkownika (int).</returns>
        public static int getUserId(HttpRequestHeaders header)
        {
            string userName = getUserName(header);
            using (var db = new WTIEntities())
            {
                var query = from user in db.Uzytkownicies
                            where user.Login==userName
                            select user.Id_uzytkownika;
                return query.First();
            }// using
        }//getUserId()
    }//AuthorizationHelper
}