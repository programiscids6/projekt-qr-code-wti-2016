﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serwer.Helpers
{
    /// <summary>
    /// Klasa pomocnicza zawierająca dane użytkownika przesłane przez aplikację mobilną.
    /// </summary>
    public class UserModel
    {
        public string username;
        public string password;
    }
}