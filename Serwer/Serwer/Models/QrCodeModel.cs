﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serwer.Models
{
    public class QrCodeModel
    {
        public System.Guid codeId { get; set; }
        public int userId { get; set; }
        public double amount { get; set; }
        public int pin { get; set; }
        public string description { get; set; }
        public DateTime date { get; set; }
    }
}