﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serwer.Models
{
    public class TransactionModel
    {
        public int transId { get; set; }
        public System.Guid codeId { get; set; }
        public double amount { get; set; }
        public int pin { get; set; }
        public string description { get; set; }
        public DateTime date { get; set; }
        public int ownerId { get; set; }
        public int customerId { get; set; }
    }
}