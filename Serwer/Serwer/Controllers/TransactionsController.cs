﻿using Serwer.Filters;
using Serwer.Helpers;
using Serwer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Serwer.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class TransactionsController : ApiController
    {
        /// <summary>
        /// GET: api/Transactions
        /// 
        /// Pobieranie listy transakcji.
        /// </summary>
        /// <returns>Lista transakcji</returns>
        public async Task<HttpResponseMessage> Get()
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                try
                {
                    int userId = AuthorizationHelper.getUserId(Request.Headers);
                    using (var db = new WTIEntities())
                    {
                        var transactions = (from transaction in db.zuzyties
                                            where userId == transaction.Id_wlasciciela || userId == transaction.Id_kupca
                                            select new
                                            {
                                                transId = transaction.Id_archiwalnego,
                                                codeId = transaction.Id_QR,
                                                amount = transaction.Kwota,
                                                pin = transaction.Pin,
                                                description = transaction.Tresc,
                                                date = transaction.Data,
                                                ownerId = transaction.Id_wlasciciela,
                                                customerId = transaction.Id_kupca
                                            }).ToList();
                        return Request.CreateResponse(HttpStatusCode.OK, new
                        {
                            Transactions = transactions
                        });
                    }//using
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Get()

        /// <summary>
        /// GET: api/Transactions/{id}
        /// 
        /// Pobieranie danych o konkretnej transakcji.
        /// </summary>
        /// <returns>Konkretna transakcja</returns>
        public async Task<HttpResponseMessage> Get(int id)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                try
                {
                    int userId = AuthorizationHelper.getUserId(Request.Headers);
                    using (var db = new WTIEntities())
                    {
                        zuzyty transaction = db.zuzyties.First(t => t.Id_archiwalnego == id && (t.Id_wlasciciela == userId || t.Id_kupca == userId));
                        if (transaction != null)
                        {
                            TransactionModel transToSend = new TransactionModel
                            {
                                transId = transaction.Id_archiwalnego,
                                codeId = (System.Guid)transaction.Id_QR,
                                amount = (double)transaction.Kwota,
                                pin = (int)transaction.Pin,
                                description = transaction.Tresc,
                                date = (DateTime)transaction.Data,
                                ownerId = (int)transaction.Id_wlasciciela,
                                customerId = (int)transaction.Id_kupca
                            };

                            return Request.CreateResponse(HttpStatusCode.OK, transToSend);
                        }//if

                        return Request.CreateResponse(HttpStatusCode.OK, Responses.ACCESS_DENIED);
                    }//using
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Get()

        /// <summary>
        /// POST: api/Transaction
        /// 
        /// Wykonywanie transakcji
        /// </summary>
        /// <param name="qrcode">Kod, którego użytkownik chce użyć</param>
        /// <returns>Success lub komunikat o błędzie</returns>
        public async Task<HttpResponseMessage> Post([FromBody]QrCodeModel qrcode)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);
                try
                {
                    using (var db = new WTIEntities())
                    {
                        Aktywny activeCode = db.Aktywnies.First(a => a.Id_QR == qrcode.codeId && a.Pin == qrcode.pin);


                        int ownerId = (int)activeCode.Id_uzytkownika;
                        // usuwanie kodu z aktywnych
                        db.Aktywnies.Remove(activeCode);
                        // zapisanie zmian w bazie
                        db.SaveChanges();

                        // aktualizacja kwoty użytkownika (przelanie środków)
                        var updatedUser = db.Uzytkownicies.First(u => u.Id_uzytkownika == userId);
                        updatedUser.Aktualna_kwota += activeCode.Kwota;
                        db.Uzytkownicies.Attach(updatedUser);
                        var entry = db.Entry(updatedUser);
                        entry.Property(e => e.Aktualna_kwota).IsModified = true;

                        // dodanie kodu do zużytych
                        zuzyty usedCode = new zuzyty
                        {
                            Kwota = (int)activeCode.Kwota,
                            Id_QR = activeCode.Id_QR,
                            Data = DateTime.Now,
                            Id_kupca = userId,
                            Id_wlasciciela = ownerId,
                            Ilosc = 0,
                            Pin = qrcode.pin,
                            Tresc = qrcode.description
                        };
                        db.zuzyties.Add(usedCode);

                        // zapisanie zmian w bazie
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                    }//using                    
                }//try
                catch (InvalidOperationException)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Błędny kod QR lub błędny pin");
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Post()
    }
}
