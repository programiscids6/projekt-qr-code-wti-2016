﻿using Serwer.Filters;
using Serwer.Helpers;
using Serwer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Serwer.Controllers
{
    // TODO: Przechowywanie hasła jako skrótu

    /// <summary>
    /// Kontroler konta. Umożliwia zarządzanie kontem (logowanie, rejestrację i modyfikację danych)
    /// </summary>
    public class AccountController : ApiController
    {
        /// <summary>
        /// Logowanie
        /// </summary>
        /// <returns>Łańcuch znaków: 
        ///  - Success w przypadku udanego logowania
        ///  - informację o błędzie gdy dane są niepoprawne</returns>
        [IdentityBasicAuthentication]
        [Authorize]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
        }//Get()

        /// <summary>
        /// Rejestracja
        /// </summary>
        /// <param name="user">Obiekt klasy pomocniczej UserModel zawierający dane o użytkowniku</param>
        /// <returns></returns>
        [Route("api/account/register")]
        [HttpPost]
        public HttpResponseMessage Register(UserModel user)
        {
            if (string.IsNullOrEmpty(user.username))
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Login nie może być pusty!");
            if (string.IsNullOrEmpty(user.password))
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Hasło nie może być puste!");

            // insert
            using (var db = new WTIEntities())
            {
                string existStrUser = null;
                var users = db.Set<Uzytkownicy>();
                try
                {
                    string checkUniq = (from existUser in users
                                        where existUser.Login == user.username
                                        select existUser).First().ToString();
                    existStrUser = checkUniq;
                }
                catch (Exception)
                {
                    existStrUser = null;
                }//catch

                if (existStrUser == null)
                {
                    users.Add(new Uzytkownicy { Login = user.username, Haslo = user.password, Aktualna_kwota = 1000 });

                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//if
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.LOGIN_IN_USE);
                }//else
            }//using
        }//Register()

        /// <summary>
        /// PUT api/account
        /// 
        /// Zmiana nazwy użytkownika
        /// 
        /// Dane wejściowe:
        /// {
        ///   "username":"user",
        ///    "password":"haslo"
        /// }
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [HttpPut]
        [IdentityBasicAuthentication]
        [Authorize]
        public HttpResponseMessage Put(UserModel userModel)
        {
            if (string.IsNullOrEmpty(userModel.username))
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Login nie może być pusty!");
            if (string.IsNullOrEmpty(userModel.password))
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Hasło nie może być puste!");

            using (var db = new WTIEntities())
            {
                if (AuthorizationHelper.getUserName(Request.Headers) == userModel.username)
                {
                    int userID = AuthorizationHelper.getUserId(Request.Headers);
                    var updatedUser = new Uzytkownicy { Id_uzytkownika = userID, Login = userModel.username, Haslo = userModel.password };
                    db.Uzytkownicies.Attach(updatedUser);
                    var entry = db.Entry(updatedUser);
                    entry.Property(e => e.Haslo).IsModified = true;
                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//if
            }//using
            return Request.CreateResponse(HttpStatusCode.OK, Responses.ACCESS_DENIED);
        }//Put()

        /// <summary>
        /// GET: api/account/balance
        /// 
        /// Zwraca stan konta aktualnego użytkownika
        /// </summary>
        /// <returns>Stan konta lub komunikat o błędzie</returns>
        [HttpGet]
        [IdentityBasicAuthentication]
        [Authorize]
        [Route("api/account/balance")]
        public HttpResponseMessage myBalance()
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            try
            {
                using (var db = new WTIEntities())
                {
                    double balance = (double)db.Uzytkownicies.First(u => u.Id_uzytkownika == userId).Aktualna_kwota;
                    return Request.CreateResponse(HttpStatusCode.OK, balance);
                }//using
            }//try
            catch (InvalidOperationException)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Niewłaściwy użytkownik lub brak środków na koncie");
            }//catch
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Błąd wewnętrzny serwera");
            }//catch
        }//myBalance()
    }
}