﻿using Serwer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Serwer.Controllers
{
    public class TestController : ApiController
    {
        /// <summary>
        /// GET api/test
        /// 
        /// Zwraca listę użytkowników z bazy w JSON-ie.
        /// </summary>
        /// <returns>Użytkownicy jako JSON</returns>
        public HttpResponseMessage Get()
        {
            using(var db = new WTIEntities())
            {
                var query = (from user in db.Uzytkownicies
                            select new
                            {
                                user.Id_uzytkownika,
                                user.Login,
                                user.Haslo
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    uzytkownicy = query
                });
            }
            // TODO: obsługa błędów (Odczyt z bazy)
        }
    }
}
