﻿using Serwer.Filters;
using Serwer.Helpers;
using Serwer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Serwer.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class QrCodeController : ApiController
    {
        /// <summary>
        /// GET: api/QrCode
        /// 
        /// Wysyłanie listy kodów należących do danego użytkownika
        /// </summary>
        /// <returns>Lista kodów
        /// Zwraca:
        /// {
        ///     "codeId": idKodu(System.Guid),
        ///     "ownername": nazwaWłaściciela(string),
        ///     "amount": kwota(double),
        ///     "description": opis(string),
        ///     "date": data(DateTime)
        /// }
        /// </returns>
        public async Task<HttpResponseMessage> Get()
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                try
                {
                    // pobieranie identyfikatora użytkownika
                    int userId = AuthorizationHelper.getUserId(Request.Headers);
                    using (var db = new WTIEntities())
                    {
                        //  lista kodów QR danego użytkownika
                        var qrcodes = (from code in db.Aktywnies
                                       where userId == code.Id_uzytkownika
                                       select new
                                       {
                                           codeId = code.Id_QR,
                                           amount = code.Kwota,
                                           pin = code.Pin,
                                           description = code.Tresc,
                                           date = code.Data
                                       }).ToList();
                        return Request.CreateResponse(HttpStatusCode.OK, new
                        {
                            QRCodes = qrcodes
                        });
                    }//using
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Get()

        /// <summary>
        /// POST api/qrcode/codeinfo, przyjmuje JSON: 
        /// {
        ///     "codeId": idKodu(System.Guid)
        /// }
        /// 
        /// Zwraca dane na temat konkretnego kodu (bez jego pinu!) każdemu uzytkownikowi, który zna jego id.
        /// Zwraca:
        /// {
        ///     "codeId": idKodu(System.Guid),
        ///     "ownername": nazwaWłaściciela(string),
        ///     "amount": kwota(double),
        ///     "description": opis(string),
        ///     "date": data(DateTime)
        /// }
        /// </summary>
        /// <param name="qrcodereqest"></param>
        /// <returns></returns>
        [Route("api/qrcode/codeinfo")]
        [HttpGet]
        public HttpResponseMessage codeInfo()
        {
            try
            {
                // pobieranie identyfikatora użytkownika
                using (var db = new WTIEntities())
                {
                    //  lista kodów QR danego użytkownika
                    var qrcodes = (from code in db.Aktywnies
                                   select new
                                   {
                                       codeId = code.Id_QR,
                                       amount = code.Kwota,
                                       pin = code.Pin,
                                       description = code.Tresc,
                                       date = code.Data
                                   }).ToList();
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        QRCodes = qrcodes
                    });
                }//using
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Get()

        /// <summary>
        /// POST: api/QrCode
        /// 
        /// Dodawanie nowego kodu QR
        /// 
        /// </summary>
        /// <param name="qrcode">Kod QR, który należy dodać do bazy
        /// {
        ///     "amount": kwota,
        ///     "pin": pin,
        ///     "description": "Tytułem - opis kodu"
        /// }
        /// </param>
        /// <returns>Zaktualizowane dane kodu (bez pin) jeśli się powiedzie lub informacje o błędzie
        /// {
        ///     "codeId": idKodu(System.Guid),
        ///     "userId": idUżytkownika,
        ///     "amount": kwota,
        ///     "description": "Tytułem",
        ///     "date": "data"
        /// }
        /// </returns>
        public async Task<HttpResponseMessage> Post([FromBody] QrCodeModel qrcode)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);
                try
                {
                    if (qrcode.amount < 0)
                        return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Kwota nie może być mniejsza od zera.");
                    if (!areMoneyAvaliable(userId, qrcode.amount))
                        return Request.CreateResponse(HttpStatusCode.OK, Responses.NOT_ENOUGH_MONEY);
                    using (var db = new WTIEntities())
                    {
                        Aktywny activeCode = new Aktywny
                        {
                            Id_uzytkownika = userId,
                            Ilosc = 1,
                            Data = DateTime.Now,
                            Kwota = qrcode.amount,
                            Tresc = qrcode.description,
                            Pin = qrcode.pin
                        };

                        // dodanie kodu
                        db.Aktywnies.Add(activeCode);
                        // aktualizacja kwoty użytkownika (blokada środków)
                        var updatedUser = db.Uzytkownicies.First(u => u.Id_uzytkownika == userId);
                        updatedUser.Aktualna_kwota -= activeCode.Kwota;
                        db.Uzytkownicies.Attach(updatedUser);
                        var entry = db.Entry(updatedUser);
                        entry.Property(e => e.Aktualna_kwota).IsModified = true;
                        // zapisanie zmian w bazie
                        db.SaveChanges();

                        // aktualizacja danych o dodanym kodzie
                        qrcode.userId = userId;
                        qrcode.codeId = activeCode.Id_QR;
                        qrcode.date = (DateTime)activeCode.Data;
                    }//using

                    // odesłanie do aplikacji pełnych danych o kodzie
                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        qrcode.codeId,
                        qrcode.userId,
                        qrcode.amount,
                        qrcode.description,
                        qrcode.date
                    });
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
                }
            });
        }//Post()

        /// <summary>
        /// Czy uzytkownik ma wystarczającą ilość środków na koncie?
        /// </summary>
        /// <param name="userId">ID użytkownika</param>
        /// <param name="amount">Żądana ilość pieniędzy</param>
        /// <returns>true - tak, false - nie</returns>
        private bool areMoneyAvaliable(int userId, double amount)
        {
            using (var db = new WTIEntities())
            {
                Uzytkownicy user = db.Uzytkownicies.First(u => u.Id_uzytkownika == userId);
                if (user.Aktualna_kwota >= amount)
                    return true;
            }//using
            return false;
        }//areMoneyAvaliable()

        /// <summary>
        /// DELETE: api/QrCode
        /// 
        /// Usuwanie kodu QR o podanym ID (ID przekazywane w ciele nagłówka - w formacie JSON)
        /// </summary>
        /// <param name="qrcode">Kod do usunięcia
        /// {
        /// 	"codeId":idKodu
        /// }
        /// </param>
        /// <returns>Success lub komunikat o błędzie</returns>
        public async Task<HttpResponseMessage> Delete([FromBody] QrCodeModel qrcode)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                try
                {
                    int userId = AuthorizationHelper.getUserId(Request.Headers);
                    using (var db = new WTIEntities())
                    {
                        Aktywny toDel = db.Aktywnies.First(e => e.Id_QR == qrcode.codeId);
                        if (toDel.Id_uzytkownika != userId)
                            return Request.CreateResponse(HttpStatusCode.OK, Responses.ACCESS_DENIED);

                        // usuwanie kodu QR
                        db.Aktywnies.Remove(toDel);
                        // aktualizacja kwoty użytkownika (odblokowanie środków)
                        var updatedUser = db.Uzytkownicies.First(u => u.Id_uzytkownika == userId);
                        updatedUser.Aktualna_kwota += toDel.Kwota;
                        db.Uzytkownicies.Attach(updatedUser);
                        var entry = db.Entry(updatedUser);
                        entry.Property(e => e.Aktualna_kwota).IsModified = true;
                        // zapisanie zmian
                        db.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                    }//using
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
                }//catch

            });
        }//Delete()
    }
}
