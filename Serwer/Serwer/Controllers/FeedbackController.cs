﻿using Serwer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Serwer.Controllers
{
    public class FeedbackController : ApiController
    {
        List<FeedbackModel> feedbackList = new List<FeedbackModel>();

        public HttpResponseMessage Post([FromBody]FeedbackModel feedback)
        {
            feedbackList.Add(feedback);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
