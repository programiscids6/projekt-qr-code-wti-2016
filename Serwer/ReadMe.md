# README #


### Serwer ###

Serwer jest opublikowany na Azure pod adresem https://wtiserver.azurewebsites.net/


### Baza danych ###

Używam bazy danych na serwerze Azure u Mikołaja. Nic nie trzeba robić.


### Zaimplementowane funkcje serwera ###

 * logowanie i zarządzanie konami użytkowników - api/account
 * zarządzanie kodami QR (pobieranie aktywnych, generowanie i usuwanie) - api/qrcode
 * wykonywanie transakcji (pobieranie historii i wykonywanie transakcji) - api/transactions
 
 
### API serwera ###

 * GET api/test - lista użytkowników
 
Logowanie i zarządzanie kontami:
 * GET api/account - logowanie
 * POST api/account/register - rejestracja, przyjmuje JSON:
	{
		"username":"login",
		"password":"haslo"
	}
 * PUT api/account - zmiana hasła, dane wejściowe JSON:
	{
		"username":"login",
		"password":"haslo"
	}
 * GET api/account/balance - informacje o aktualnym stanie kont
	zwraca: aktualny stan konta (kwotę double)

Zarządzenie kodami QR:
 * GET api/qrcode - lista kodów danego użytkownika
 * POST api/qrcode/codeinfo - Zwraca dane na temat konkretnego kodu (bez jego pinu!) każdemu uzytkownikowi, który zna jego id.
		Przyjmuje JSON: 
         {
             "codeId": idKodu(System.Guid)
         }
         
         
         Zwraca:
         {
             "codeId": idKodu(System.Guid),
             "ownername": nazwaWłaściciela(string),
             "amount": kwota(double),
             "description": opis(string),
             "date": data(DateTime)
         }
		 
 * POST api/qrcode - dodanie nowego kodu, przyjmuje
	{
		"amount": kwota,
		"pin": pin,
		"description": "Tytułem - opis kodu"
	}	
	
	zwraca
	{
		"codeId": idKodu(System.Guid),
		"userId": idUżytkownika,
		"amount": kwota,
		"description": "Tytułem",
		"date": "data"
	}
	
 * DELETE api/qrcode - usuwanie kodu. Usuwamy przekazując model kodu w ciele, przy czym wystarczy ID kodu:
		{
			"codeId":idKodu
		}
	
Wykonywanie transakcji:
 * GET api/transactions - pobieranie listy transakcji z udziałem danego użytkownika
 * GET api/transactions/{id} - pobieranie konkretnej transakcji
 * POST api/transactions - wykonywanie transakcji, przyjmuje:
	{
		"codeId": idKodu,
		"pin":pin	
	}