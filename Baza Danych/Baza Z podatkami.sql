-- ----------------
-- czyszczenie bazy
-- ----------------
use WTI
drop trigger podatki
IF OBJECT_ID('Aktywny') IS NOT NULL
DROP TABLE Aktywny;
IF OBJECT_ID('zuzyty') IS NOT NULL
DROP TABLE zuzyty;
IF OBJECT_ID('Ogolne') IS NOT NULL
DROP TABLE Ogolne;
IF OBJECT_ID('Uzytkownicy') IS NOT NULL
DROP TABLE Uzytkownicy;

use [WTI]
-- zmiana kodowania na obsługujące język polski 
ALTER DATABASE [WTI] COLLATE Polish_CI_AS
GO

-- tabela z danymi uzytkowników
Create Table Uzytkownicy(
Id_uzytkownika int Primary Key Identity(1,1),
Aktualna_kwota float,
Login nvarchar(30),
Haslo  nvarchar(30));


--tabela z Qrcodami stworzonymi ale jeszcze nie wykorzystanymi
Create table Aktywny(
Id_QR UNIQUEIDENTIFIER DEFAULT NEWID() PRIMARY KEY,
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Kwota float,
Ilosc int,
Pin int ,
Tresc nvarchar(200),
Data date);

-- tabela z wykorzystanymi juz Qrcodami 
Create table zuzyty(
Id_archiwalnego int Primary key Identity(1,1),
Id_QR UNIQUEIDENTIFIER,
Kwota float,
Pin int,
Tresc nvarchar(200),
Ilosc int,
Data date,
Id_wlasciciela int references Uzytkownicy(Id_uzytkownika),
Id_kupca int references Uzytkownicy(Id_uzytkownika));

Create table Ogolne(
Klucz int primary key,
ilosc_uzytkownikow int,
ilosc_aktywnychQR int,
Kwota_z_podatku float);

-- przykładowy użytkownik
INSERT INTO Uzytkownicy VALUES(980, 'Jan', 'haslo');
SELECT * FROM Uzytkownicy;

-- przykładowy aktywny kod QR
INSERT INTO Aktywny VALUES(DEFAULT, 1, 20, 1, 1234, 'opis', '2016-05-21');
SELECT * FROM Aktywny;

SELECT * FROM zuzyty;
go

go
drop trigger podatki
create trigger podatki
on zuzyty
after insert,update
as
begin

declare 
@KwotaP float,
@KwotaU float,
@Podatek float,
@Kwotapodatku float;
Select @KwotaP = MAX(o.Kwota_z_podatku)
From Ogolne o

Select @KwotaU = SUM(u.Aktualna_kwota) + SUM(a.Kwota)
from Uzytkownicy u, Aktywny a,inserted i 
where i.Id_wlasciciela = a.Id_uzytkownika and i.Id_wlasciciela = u.Id_uzytkownika

Select @Podatek =  (@KwotaU - (450+@KwotaP))/5000
from inserted i

Select @KwotaPodatku = Round( i.Kwota*@Podatek,2)
from inserted i

if @Podatek > 0.5
begin
set @Podatek = 0.5
end
if @KwotaPodatku>@KwotaP
begin
Update Ogolne set Kwota_z_podatku = Kwota_z_podatku + @KwotaPodatku
where Ogolne.Klucz = 1

Update Uzytkownicy set Aktualna_kwota = Aktualna_kwota - @KwotaPodatku
from inserted i 
where Uzytkownicy.Id_uzytkownika = i.Id_wlasciciela
end

end


