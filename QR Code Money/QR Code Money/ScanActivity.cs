using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace QR_Code_Money
{
    [Activity(Label = "ScanActivity")]
    public class ScanActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ScanLayout);

            TextView QrCodeTextView = FindViewById<TextView>(Resource.Id.QrCodeTextView);

            string QrTekst = Intent.GetStringExtra("QrTekst") ?? "Data not available";
            QrCodeTextView.Text = QrTekst;
            // Create your application here

        }
    }
}