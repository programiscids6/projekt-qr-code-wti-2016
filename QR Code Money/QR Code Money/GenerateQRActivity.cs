using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ZXing;
using Android.Graphics;
using ZXing.Mobile;
//using ZXing.Mobile;
//using Android.Graphics;

namespace QR_Code_Money
{
    [Activity(Label = "GenerateQRActivity")]
    public class GenerateQRActivity : Activity
    {
        string text;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.GenerateQRLayout);
            // Create your application here

            ImageView view = FindViewById<ImageView>(Resource.Id.QRImageView);
            Button generateButton = FindViewById<Button>(Resource.Id.QrGenerateButton);
            EditText textField = FindViewById<EditText>(Resource.Id.QrGenerateEditText);
            text = textField.Text;            

            /* generateButton.Click += async (sender, e) => {
                 var scanner = new ZXing.Mobile.MobileBarcodeScanner(this);
                 var result = await scanner.Scan();

                 //Console.WriteLine(result.Text);
                 Toast.MakeText(this, result.Text, ToastLength.Long).Show();
             };*/

            generateButton.Click +=  (sender, e) => {
                if (String.IsNullOrEmpty(textField.Text))
                {
                    Toast.MakeText(this, "Write some text to hide in QR Code", ToastLength.Long).Show();
                }
                else
                {
                    text = textField.Text;
                    view.SetImageBitmap(GetQRCode());
                    view.Visibility = ViewStates.Visible;
                    //Console.WriteLine(result.Text);
                    Toast.MakeText(this, "Succesfull generation", ToastLength.Long).Show();
                }                
            };

        }

        private Bitmap GetQRCode()
        {
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new ZXing.Common.EncodingOptions
                {
                    Height = 600,
                    Width = 600
                }
            };
            
            return writer.Write(text);
        }
    }
}