using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace QR_Code_Money
{
    [Activity(Label = "WelcomeActivity")]
    public class WelcomeActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here

            SetContentView(Resource.Layout.WelcomeLayout);

            string login = Intent.GetStringExtra("Login") ?? "Data not available";

            TextView LoginTextView = FindViewById<TextView>(Resource.Id.LoginTextView);
            Button generateButton = FindViewById<Button>(Resource.Id.generateButton);
            Button scanButton = FindViewById<Button>(Resource.Id.scanQrButton);

            LoginTextView.Text = "Witaj, " + login + "!";

            generateButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(GenerateQRActivity));                
                StartActivity(intent);
            };

            scanButton.Click += async (sender, e) =>
            {
                var scanner = new ZXing.Mobile.MobileBarcodeScanner(this);
                var result = await scanner.Scan();

                var intent = new Intent(this, typeof(ScanActivity));
                intent.PutExtra("QrTekst", result.Text);
                StartActivity(intent);

                //Console.WriteLine(result.Text);
                //Toast.MakeText(this, result.Text, ToastLength.Long).Show();
            };

        }
    }
}