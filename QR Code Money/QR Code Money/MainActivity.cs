﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace QR_Code_Money
{
    [Activity(Label = "QR Code Money", MainLauncher = true, Icon = "@drawable/Wallet")]
    public class MainActivity : Activity
    {
       // int count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button loginButton = FindViewById<Button>(Resource.Id.loginButton);
            EditText loginText = FindViewById<EditText>(Resource.Id.loginText);


            loginButton.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(WelcomeActivity));
                intent.PutExtra("Login", loginText.Text);
                StartActivity(intent);
            };
        }
    }
}

